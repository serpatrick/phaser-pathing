export default interface VectorLike {
  x: number;
  y: number;
}
