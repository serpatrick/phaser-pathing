import * as EasyStar from 'easystarjs';
import { Array2D } from 'reeks2d';
import { clamp } from 'nuttig';
import Vector from '../utils/Vector.js';
import VectorLike from '../types/VectorLike.js';

interface Options {
  width: number;
  height: number;
  scale: number;
}

export default class Pathing {
  easystar: EasyStar.js;
  grid: Array2D<number>;
  scale: number;

  constructor({ width, height, scale }: Options) {
    this.scale = scale;
    this.grid = Array2D.create(width, height, 0);
    this.easystar = new EasyStar.js();
    this.easystar.disableCornerCutting();
    this.easystar.enableDiagonals();
    this.easystar.setAcceptableTiles([0]);
    this.easystar.setGrid(this.grid.grid);
  }

  find(
    startX: number,
    startY: number,
    endX: number,
    endY: number,
    callback: (nodes: Vector[]) => void
  ) {
    const id = this.easystar.findPath(startX, startY, endX, endY, (path) => {
      if (path) {
        path.shift();

        const nodes = path.map((node) =>
          new Vector(node).scale(this.scale + 0.5)
        );

        callback(nodes);
      } else {
        callback([]);
      }
    });

    this.easystar.calculate();

    return id;
  }

  cancel(id: number) {
    this.easystar.cancelPath(id);
  }

  add(x: number, y: number, shape?: Array2D<number>) {
    if (!shape) {
      return;
    }
    this.grid.apply(shape.grid, x, y, (a, b) => a + b);
  }

  remove(x: number, y: number, shape?: Array2D<number>) {
    if (!shape) {
      return;
    }
    this.grid.apply(shape.grid, x, y, (a, b) => a - b);
  }

  isFree(x: number, y: number) {
    return this.grid.get(x, y) === 0;
  }

  isBlocked(x: number, y: number) {
    return this.grid.get(x, y) > 0;
  }

  toGridCoordinates({ x, y }: VectorLike): VectorLike {
    return {
      x: this.getGridX(x),
      y: this.getGridY(y),
    };
  }

  getGridX(x: number) {
    return clamp(Math.floor(x / this.scale), 0, this.grid.width - 1);
  }

  getGridY(y: number) {
    return clamp(Math.floor(y / this.scale), 0, this.grid.height - 1);
  }

  toWorldCoordinates({ x, y }: VectorLike): VectorLike {
    return {
      x: this.getWorldX(x),
      y: this.getWorldY(y),
    };
  }

  getWorldX(x: number) {
    return x * this.scale;
  }

  getWorldY(y: number) {
    return y * this.scale;
  }

  get worldWidth() {
    return this.grid.width * this.scale;
  }

  get worldHeight() {
    return this.grid.height * this.scale;
  }
}
